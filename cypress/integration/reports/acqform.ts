describe("reports/acqform", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("acqform", function () {
        cy.visit("/cgi-bin/koha/reports/acquisitions_stats.pl");
        cy.get("main").should('be.visible').screenshot("acqform");
    });
});
