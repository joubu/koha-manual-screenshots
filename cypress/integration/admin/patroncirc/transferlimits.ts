describe("admin/patroncirc/transferlimits", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("transferlimits", function () {
        cy.visit("/cgi-bin/koha/admin/branch_transfer_limits.pl");
        cy.get("main").should('be.visible').screenshot("transferlimits");
    });
});
