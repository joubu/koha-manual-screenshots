describe("admin/additional/usagestatistics", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("usagestatistics", function () {
        cy.visit("/cgi-bin/koha/admin/usage_statistics.pl");
        cy.get("main").should('be.visible').screenshot("usagestatistics");
    });
});
