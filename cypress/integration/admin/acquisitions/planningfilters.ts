describe("admin/acquisitions/planningfilters", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("planningfilters", function () {
        cy.visit("/cgi-bin/koha/admin/aqplan.pl?budget_period_id=1&authcat=MONTHS");
        cy.get("#filter_planning").should('be.visible').screenshot("planningfilters");
    });
});
