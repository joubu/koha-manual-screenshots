describe("cataloging/advancedcataloging", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("advancedcataloging", function () {
        cy.visit("/cgi-bin/koha/cataloguing/editor.pl");
        cy.get("main").should('be.visible').screenshot("advancedcataloging");
    });
});
