import { defineConfig } from "cypress";

export default defineConfig({
    fixturesFolder: "cypress/fixtures",
    screenshotsFolder: "cypress/screenshots",
    videosFolder: "cypress/videos",
    defaultCommandTimeout: 10000,

    e2e: {
        setupNodeEvents(on, config) {
            return require("./cypress/plugins/index.js")(on, config);
        },
        experimentalStudio: true,
        experimentalRunAllSpecs: true,
        viewportWidth: 1280,
        viewportHeight: 800,
        baseUrl: "http://kohadev.mydnsname.org:8081",
        specPattern: "cypress/integration/**/*.*",
        supportFile: "cypress/support/e2e.js",
        env: {
            db: {
                host: "db",
                user: "koha_kohadev",
                password: "password",
                database: "koha_kohadev",
            },
        },
    },

    component: {
        devServer: {
            //framework: "vue-cli",
            bundler: "webpack",
        },
    },
});
