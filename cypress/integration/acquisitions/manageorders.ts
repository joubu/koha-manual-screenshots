describe("acquisitions/manageorders", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("manageorders", function () {
        cy.visit("/cgi-bin/koha/acqui/acqui-home.pl");
        cy.get("div#acqui_acqui_home_order").should('be.visible').screenshot("manageorders");
    });
});
