describe("admin/acquisitions/nounreceivedorders", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("nounreceivedorders", function () {
        cy.visit("/cgi-bin/koha/admin/aqbudgetperiods.pl?op=close_form&budget_period_id=1");
        cy.get("main").should('be.visible').screenshot("nounreceivedorders");
    });
});
