use Modern::Perl;

use utf8;
use Encode qw( encode_utf8 decode_utf8 );
use Text::CSV::Encoded;
use POSIX qw( strftime );
use IPC::Cmd qw( run );
use List::Util qw(first);

my $list_of_images = "list_of_images.csv";

my $csv = Text::CSV::Encoded->new(
    {
        encoding_out => 'utf8',
        encoding_in  => 'utf8',
        binary       => 1,
        quote_char   => q{"},
        sep_char     => ',',
    }
);

open (my $in,  $list_of_images );
while( my $columns = $csv->getline( $in ) ) {
    my ( $filename, $module, $image_url, $difficulty, $page_on_koha, $element_id, $missing_selector, $workflow, $note, $done_by ) = @$columns;
    my $filepath = sprintf('cypress/screenshots/%s/%s.ts/en_%s.png', $module, $filename, $filename );
    #say sprintf('=HYPERLINK("https://gitlab.com/koha-community/koha-manual/-/raw/main/source/images/%s/%s.png", "%s")', $module, $filename, $filename);
    if ( -e $filepath ) {
        say sprintf('=HYPERLINK("https://gitlab.com/joubu/koha-manual-screenshots/-/raw/main/%s", "%s")', $filepath, $filename);
    } else {
        say '';
    }
}
close($in);
