describe("admin/patroncirc/showattribute", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("showattribute", function () {
        cy.visit("/cgi-bin/koha/members/moremember.pl?borrowernumber=19");
        cy.get(".patroninfo").should('be.visible').screenshot("showattribute");
    });
});
