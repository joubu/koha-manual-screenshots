describe("cataloging/addmultiplecopies", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addmultiplecopies", function () {
        cy.visit("/cgi-bin/koha/cataloguing/additem.pl?biblionumber=1");
        cy.get("#add_multiple_copies").click();
        cy.get("#add_multiple_copies_span").should('be.visible').screenshot("addmultiplecopies");
    });
});
