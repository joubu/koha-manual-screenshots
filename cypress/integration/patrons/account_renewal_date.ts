describe("patrons/account_renewal_date", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("account_renewal_date", function () {
        cy.visit("/cgi-bin/koha/members/moremember.pl?borrowernumber=19");
        cy.get("#patron-library-details").should('be.visible').screenshot("account_renewal_date");
    });
});
