describe("acquisitions/newvendor2", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newvendor2", function () {
        cy.visit("/cgi-bin/koha/acqui/supplier.pl?op=enter");
        cy.get("#contacts").should('be.visible').screenshot("newvendor2");
    });
});
