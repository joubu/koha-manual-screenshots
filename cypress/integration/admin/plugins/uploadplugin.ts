describe("admin/plugins/uploadplugin", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("uploadplugin", function () {
        cy.visit("/cgi-bin/koha/plugins/plugins-upload.pl?op=Upload");
        cy.get(".col-sm-6").should('be.visible').screenshot("uploadplugin");
    });
});
