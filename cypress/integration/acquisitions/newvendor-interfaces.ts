describe("acquisitions/newvendor-interfaces", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newvendor-interfaces", function () {
        cy.visit("/cgi-bin/koha/acqui/supplier.pl?op=enter");
        cy.get("#interfaces").should('be.visible').screenshot("newvendor-interfaces");
    });
});
