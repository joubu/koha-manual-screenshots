describe("circ/transfer", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("transfer", function () {
        cy.visit("/cgi-bin/koha/circ/branchtransfers.pl");
        cy.get("main").should('be.visible').screenshot("transfer");
    });
});
