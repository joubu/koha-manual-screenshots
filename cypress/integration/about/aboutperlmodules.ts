describe("about/aboutperlmodules", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("aboutperlmodules", function () {
        cy.visit("/cgi-bin/koha/about.pl#perl_panel");
        cy.get("#perl_panel").should('be.visible').screenshot("aboutperlmodules");
    });
});
