// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

function get_fallback_login_value(param) {

    var env_var = param == 'username' ? 'KOHA_USER' : 'KOHA_PASS';

    return typeof Cypress.env(env_var) === 'undefined' ? 'koha' : Cypress.env(env_var);
}

Cypress.Commands.add('login', (username, password) => {
    var user = typeof username === 'undefined' ? get_fallback_login_value('username') : username;
    var pass = typeof password === 'undefined' ? get_fallback_login_value('password') : password;
    cy.visit('/cgi-bin/koha/mainpage.pl?logout.x=1')
    cy.get("#userid").type(user)
    cy.get("#password").type(pass)
    cy.get("#submit-button").click()
})

const mysql = require('cypress-mysql');
mysql.addCommands();

const langs = {
    'en'        : '',
    'ar-Arab'   : '.ar',
    'cs-CZ'     : '.cs',
    'de-DE'     : '.de',
    'es-ES'     : '.es',
    'fr-FR'     : '.fr',
    'fr-CA'     : '.fr_CA',
    'hi'        : '.hi',
    'it-IT'     : '.it',
    'pt-PT'     : '.pt',
    'pt-BR'     : '.pt_BR',
    'sk-SK'     : '.sk',
    'sv-SE'     : '.sv',
    'tr-TR'     : '.tr',
    'zh-Hant-TW': '.zh_TW',
};
Cypress.Commands.overwrite(
  'screenshot',
  (originalFn, subject, name, options = {}) => {
    const lang = Cypress.env('KOHA_LANG') || 'en';
    name += langs[lang];
    options.overwrite = true;
    return originalFn(subject, name, options)
  }
)

Cypress.Commands.add('wait_for_ajax', () => {
    cy.intercept('/**').as('ajax-requests');
    cy.wait('@ajax-requests');
})
Cypress.Commands.add('set_cookie_lang', () => {
    const lang = Cypress.env("KOHA_LANG") || "en";
    cy.setCookie("KohaOpacLanguage", lang);
})

Cypress.Commands.add('set_syspref', (variable, value) => {
    //let original_value = "";
    //cy.query('SELECT value FROM systempreferences WHERE variable=?', variable).then(res => {
    //    original_value = res
    //});
    const csrfToken = Cypress.$("meta[name='csrf-token']").attr("content");

    let sessid = '';
        cy.getCookies().then(cookies => {
            const c = cookies.find(c => c.name == 'CGISESSID')
            sessid = c.value
        }).then(() => {
            console.log(sessid, "sessid");
    cy.request({
        method: 'POST',
        url: 'http://kohadev-intra.mydnsname.org:8081/cgi-bin/koha/svc/config/systempreferences',
        form: true,
        body: "pref_" + variable + "=" + value + "&csrf_token=" + csrfToken,
        headers: {
          'Cookie': 'CGISESSID=' + sessid
        },
    });
        });
})

Cypress.Commands.add('show_patron', (patron_id) => {
    cy.visit("/cgi-bin/koha/members/moremember.pl?borrowernumber=37")
});
Cypress.Commands.add('edit_patron', (patron_id) => {
    cy.visit("/cgi-bin/koha/members/memberentry.pl?op=modify&destination=circ&borrowernumber=37")
});
Cypress.Commands.add('checkout', (patron_id, barcode) => {
    cy.visit("/cgi-bin/koha/circ/circulation.pl?borrowernumber=37")
    cy.get("#barcode").type(barcode);
    cy.get("#circ_circulation_issue > .btn").click();
});
Cypress.Commands.add('checkin', (barcode) => {
    cy.visit("/cgi-bin/koha/circ/returns.pl")
    //cy.get("#ret_barcode").clear();
    cy.get("#barcode").type(barcode);
    cy.get("#checkin-form button[type='submit']").click();
});
