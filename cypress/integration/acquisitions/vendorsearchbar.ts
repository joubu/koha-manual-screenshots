describe("acquisitions/vendorsearchbar", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("vendorsearchbar", function () {
        cy.visit("/cgi-bin/koha/acqui/acqui-home.pl");
        cy.get("#header_search").should('be.visible').screenshot("vendorsearchbar");
    });
});
