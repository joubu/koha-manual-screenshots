describe("reports/dict1", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("dict1", function () {
        cy.visit("/cgi-bin/koha/reports/dictionary.pl?phase=Add New Definition");
        cy.get("main").should('be.visible').screenshot("dict1");
    });
});
