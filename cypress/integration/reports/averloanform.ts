describe("reports/averloanform", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("averloanform", function () {
        cy.visit("/cgi-bin/koha/reports/issues_avg_stats.pl");
        cy.get("main").should('be.visible').screenshot("averloanform");
    });
});
