describe("tools/additionalcontent/additionalcontent", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("additionalcontent", function () {
        cy.visit("/cgi-bin/koha/tools/additional-contents.pl?category=html_customizations");
        cy.get("main").should('be.visible').screenshot("additionalcontent");
    });
});
