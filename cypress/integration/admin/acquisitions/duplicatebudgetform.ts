describe("admin/acquisitions/duplicatebudgetform", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("duplicatebudgetform", function () {
        cy.visit("/cgi-bin/koha/admin/aqbudgetperiods.pl?op=duplicate_form&budget_period_id=1");
        cy.get("main").should('be.visible').screenshot("duplicatebudgetform");
    });
});
