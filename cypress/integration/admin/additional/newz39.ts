describe("admin/additional/newz39", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newz39", function () {
        cy.visit("/cgi-bin/koha/admin/z3950servers.pl?op=add&type=zed");
        cy.get("main").should('be.visible').screenshot("newz39");
    });
});
