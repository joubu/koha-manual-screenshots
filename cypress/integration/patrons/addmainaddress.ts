describe("patrons/addmainaddress", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addmainaddress", function () {
        cy.visit("/cgi-bin/koha/members/memberentry.pl?op=add&categorycode=PT");
        cy.get("#memberentry_mainaddress").should('be.visible').screenshot("addmainaddress");
    });
});
