describe("admin/additional/newidentityprovider", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newidentityprovider", function () {
        cy.visit("/cgi-bin/koha/admin/identity_providers.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("newidentityprovider");
    });
});
