describe("admin/basicparams/addlibgroup", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addlibgroup", function () {
        cy.visit("/cgi-bin/koha/admin/library_groups.pl");
        cy.get("#add-group-root").click();
        cy.get("#add-group-form .modal-content").should('be.visible').screenshot("addlibgroup");
    });
});
