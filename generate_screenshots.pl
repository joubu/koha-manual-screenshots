use Modern::Perl;

use utf8;
use Encode qw( encode_utf8 decode_utf8 );
use Text::CSV::Encoded;
use POSIX qw( strftime );
use IPC::Cmd qw( run );
use List::Util qw(first);
use Getopt::Long qw( GetOptions );
use Pod::Usage qw( pod2usage );
use File::Slurp qw( read_file );
use File::Path qw( make_path );

# FIXME This needs to be run on top of patches from 35505

my ( $help, $update_csv, @ids );
GetOptions(
    'help|?'         => \$help,
    'update-csv'     => \$update_csv,
    'id=s'           => \@ids,
);

pod2usage(1) if $help;

if ( $update_csv ) {
    say "Pulling csv...";
    qx{wget 'https://docs.google.com/spreadsheets/d/1z1SRT6p9GZhc45SVYwo4dQVjJ3dS48nA6d8OFRXOEsM/gviz/tq?tqx=out:csv&sheet=Images list' -O list_of_images.csv};
}

my $existing_images;
for my $i (read_file('../koha-manual-screenshots-wip/koha_manual_images_out.txt')){
    chomp $i;
    my ($id, $path) = split ':', $i;
    $existing_images->{$id} = $path;
}

my $koha_src = "../koha/";
# Generate list_of_images.csv with:
# gdoc, File > Download > ods
# libreoffice 'List of images.ods'
# File > Save as > csv
# mv 'List of images.csv' list_of_images.csv
my $list_of_images = "list_of_images.csv";

my @scripts;
my $pl_list = run_cmd(qq{find $koha_src -name '*.pl'}, { silent => 1 } );
for my $pl ( split "\n", $pl_list->{output} ) {
    next if $pl =~ m{installer/data/mysql/db_revs/};
    next if $pl =~ m{opac}; # FIXME Support OPAC!
    ( my $script = $pl ) =~ s|$koha_src||;
    push @scripts, $script;
}

my $csv = Text::CSV::Encoded->new(
    {
        encoding_out => 'utf8',
        encoding_in  => 'utf8',
        binary       => 1,
        quote_char   => q{"},
        sep_char     => ',',
    }
);

open (my $in,  $list_of_images );
while( my $columns = $csv->getline( $in ) ) {
    my ( $filename, $module, $image_url, $new_image_url, $difficulty, $page_on_koha, $element_id, $missing_selector, $workflow, $note, $done_by ) = @$columns;

    next if @ids && !grep { $filename eq $_ } @ids;

    next if $difficulty ne 'Easy'; # Process the Easy ones first

    next unless $page_on_koha; # No page, no info

    unless ( exists $existing_images->{$filename} ) {
        warn "$filename does not appear in the list of existing images, skipping";
        next;
    }
    $module = $existing_images->{$filename};

    #say "#$filename, $module, $image_url, $difficulty, $page_on_koha, $element_id, $missing_selector, $workflow, $note, $done_by#";
    my @steps;
    my $s = $page_on_koha;
    $s =~ s|\?.*$||; # currency.pl?op=add_form
    $s =~ s|#.*$||; # columns_settings.pl#currency
    $s =~ s|^/||; # /admin/branches.pl?op=add_form
    $s =~ s|^cgi-bin/koha/||; #cgi-bin/koha/mainpage.pl
    $s =~ s|^/cgi-bin/koha/||; #/cgi-bin/koha/mainpage.pl

    my $page = first { $_ =~ m{$s} } @scripts;
    if ( $page_on_koha =~ m{(\?.*$)} ) {
        $page .= $1;
    }
    if ( $page_on_koha =~ m{(#.*$)} ) {
        $page .= $1;
    }

    unless ($page){
        warn "'$page_on_koha' => '$page' not found!";
        next;
    }

    my @sysprefs;
    push @steps, sprintf(q{cy.visit("/cgi-bin/koha/%s");}, $page);
    for my $step ( split "\n", $workflow ) {
        #cy.set_syspref("AllowStaffToSetCheckoutsVisibilityForGuarantor", "1");
        #cy.edit_patron(37); // Modify patron Lisa Charles (23529000197047) (Kid)
        #cy.get("#memberentry_guarantor")
        #    .should("be.visible")
        #    .screenshot("allowstafftosetvisibilityforguarantor");
        if ( $step =~ m{^click (.*)} ) {
            push @steps, sprintf(q{cy.get("%s").click();}, escape($1));
        } elsif ( $step =~ m{^go_to (.*)} ) {
            my $s = $1;
            my $page = first { $_ =~ m{$s} } @scripts;
            warn "Cannot find page $s" unless $page;
            push @steps, sprintf(q{cy.visit("/cgi-bin/koha/%s");}, $page);
        } elsif ( $step =~ m{set_syspref ([^=]+)=(.*)} ) {
            push @sysprefs, sprintf(q{cy.set_syspref("%s", "%s");}, escape($1), escape($2));
        }
    }
    push @steps, sprintf(q{cy.get("%s").should('be.visible').screenshot("%s");}, escape($element_id), escape($filename));
    my $steps = join "\n        ", (@sysprefs, @steps);
    my $content = <<EOF;
describe("$module", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("$filename", function () {
        $steps
    });
});
EOF

    ( my $module_path = $module ) =~ s|(.*)/\w+$|$1|;
    make_path "cypress/integration/$module_path";
    open my $fh, '>', "cypress/integration/$module.ts" or die $!;
    print $fh $content;
    close $fh;
}

close($in);

sub run_cmd {
    my $cmd        = shift;
    my $params     = shift;
    my $silent     = $params->{silent}     || 0;
    my $do_not_die = $params->{do_not_die} || 0;

    say sprintf "[%s]", strftime( "%Y-%m-%d %H:%M:%S", localtime );
    my ( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) =
      run( command => $cmd, verbose => !$silent );
    die sprintf "Command failed with %s\n", $error_code
      unless $do_not_die || $success;
    my $output = join('', map {chomp; $_} @$full_buf);
    return { output => $output, error_code => $error_code };
}

sub escape {
    my $s = shift;
    $s =~ s{"}{\\"}g;
    return $s;
}
