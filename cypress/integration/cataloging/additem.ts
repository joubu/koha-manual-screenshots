describe("cataloging/additem", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("additem", function () {
        cy.visit("/cgi-bin/koha/cataloguing/additem.pl?biblionumber=1");
        cy.get("#cataloguing_additem_newitem").should('be.visible').screenshot("additem");
    });
});
