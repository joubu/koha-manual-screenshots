describe("admin/additional/z39list", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("z39list", function () {
        cy.visit("/cgi-bin/koha/admin/z3950servers.pl");
        cy.get("main").should('be.visible').screenshot("z39list");
    });
});
