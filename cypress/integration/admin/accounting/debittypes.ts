describe("admin/accounting/debittypes", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("debittypes", function () {
        cy.visit("/cgi-bin/koha/admin/debit_types.pl");
        cy.get("main").should('be.visible').screenshot("debittypes");
    });
});
