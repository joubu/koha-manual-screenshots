describe("cataloging/additembuttons", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("additembuttons", function () {
        cy.visit("/cgi-bin/koha/cataloguing/additem.pl?biblionumber=1");
        cy.get(".action").should('be.visible').screenshot("additembuttons");
    });
});
