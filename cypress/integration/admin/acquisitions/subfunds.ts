describe("admin/acquisitions/subfunds", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("subfunds", function () {
        cy.visit("/cgi-bin/koha/admin/aqbudgets.pl?budget_period_id=1");
        cy.get("main").should('be.visible').screenshot("subfunds");
    });
});
