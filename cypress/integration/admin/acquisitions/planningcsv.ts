describe("admin/acquisitions/planningcsv", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("planningcsv", function () {
        cy.visit("/cgi-bin/koha/admin/aqplan.pl?budget_period_id=1&authcat=MONTHS");
        cy.get("#export_planning").should('be.visible').screenshot("planningcsv");
    });
});
