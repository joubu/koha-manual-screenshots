describe("cataloging/008plugin", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("008plugin", function () {
        cy.visit("/cgi-bin/koha/cataloguing/plugin_launcher.pl?plugin_name=marc21_field_008.pl");
        cy.get("body").should('be.visible').screenshot("008plugin");
    });
});
