describe("admin/acquisitions/currencies", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("currencies", function () {
        cy.visit("/cgi-bin/koha/admin/currency.pl");
        cy.get("main").should('be.visible').screenshot("currencies");
    });
});
