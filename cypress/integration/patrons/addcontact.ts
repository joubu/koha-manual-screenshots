describe("patrons/addcontact", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addcontact", function () {
        cy.visit("/cgi-bin/koha/members/memberentry.pl?op=add&categorycode=PT");
        cy.get("#memberentry_contact").should('be.visible').screenshot("addcontact");
    });
});
