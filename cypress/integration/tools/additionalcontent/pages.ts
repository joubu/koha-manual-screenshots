describe("tools/additionalcontent/pages", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("pages", function () {
        cy.visit("/cgi-bin/koha/tools/additional-contents.pl?category=pages");
        cy.get("main").should('be.visible').screenshot("pages");
    });
});
