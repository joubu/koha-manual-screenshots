describe("admin/patroncirc/patronrestrictiontypes", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("patronrestrictiontypes", function () {
        cy.visit("/cgi-bin/koha/admin/restrictions.pl");
        cy.get("main").should('be.visible').screenshot("patronrestrictiontypes");
    });
});
