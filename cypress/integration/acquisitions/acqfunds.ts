describe("acquisitions/acqfunds", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("acqfunds", function () {
        cy.visit("/cgi-bin/koha/acqui/acqui-home.pl");
        cy.get("#BudgetsAndFunds").should('be.visible').screenshot("acqfunds");
    });
});
