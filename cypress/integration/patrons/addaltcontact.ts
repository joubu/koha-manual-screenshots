describe("patrons/addaltcontact", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addaltcontact", function () {
        cy.visit("/cgi-bin/koha/members/memberentry.pl?op=add&categorycode=PT");
        cy.get("#memberentry_altaddress").should('be.visible').screenshot("addaltcontact");
    });
});
