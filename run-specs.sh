# WARNING - With trashAssetsBeforeRuns=false we keep the screenshots and cypress/results. Certainly better to not have it but move the assets between each run
if [[ -z "${KOHA_LANG}" ]]; then
    export KOHA_LANG=en
fi
xhost local:root; docker run -it \
  -v $PWD:/e2e \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -w /e2e \
  -e DISPLAY \
  --entrypoint cypress \
  --network koha_kohanet \
  cypress/included:12.17.4 run --config video=false,screenshotOnRunFailure=false,trashAssetsBeforeRuns=false --reporter mochawesome --reporter-options 'reportDir=cypress/results,reportFilename=[name]-report,overwrite=false,json=true,html=false,toConsole=true' --env KOHA_LANG=$KOHA_LANG --project . $@
